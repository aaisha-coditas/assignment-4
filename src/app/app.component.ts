import { Component, OnInit } from '@angular/core';
import {ApiService} from './api.service'
import { Forum } from './forum';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'forum';

  id:number;
  name:string;
  username:string;
  email:string;

  forum;

  constructor(private ApiService:ApiService){

  }
ngOnInit(){
  this.ApiService.getDetails().subscribe(data=>{
    this.forum=data;
  });
}


 
}
