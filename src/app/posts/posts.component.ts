import { Component, OnInit, Input, ɵNG_DIRECTIVE_DEF } from '@angular/core';
import { ApiService } from '../api.service';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(private ApiService:ApiService, private route:ActivatedRoute,private router: Router) { }
posts;


nId;
  ngOnInit(){

    this.nId=this.route.snapshot.paramMap.get('id');
    console.log(this.nId);
    this.ApiService.getPosts(this.nId).subscribe(data=>{
      this.posts=data;
    
     
    });
  }
  comments;
  getComments(id){
    //alert("ok"+id);
    alert(id);
    this.ApiService.getComments(id).subscribe(data=>{
      this.comments=data;

    //this.router.navigate(['/comments/'+id]);

  })

  }
}
