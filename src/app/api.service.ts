import { Injectable } from '@angular/core';
import {Forum} from './forum';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient:HttpClient) { }

  forum:Forum[];

  getDetails(){
    return this.httpClient.get('https://jsonplaceholder.typicode.com/users')
    
  }
  userId;
  getPosts(id){
    
    console.log(id);
    
    return this.httpClient.get('https://jsonplaceholder.typicode.com/posts?userId='+id);
    
  
  }
  getPost(){
    return this.httpClient.get('https://jsonplaceholder.typicode.com/comments');
    
  
  }
  getComments(postId){
    return this.httpClient.get('https://jsonplaceholder.typicode.com/comments?postId='+postId);

  }

}
