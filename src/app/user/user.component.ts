import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service'
import { Forum } from '../forum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  [x: string]: any;

  constructor(private ApiService:ApiService, private router: Router){

  }
users;
  ngOnInit(){
    this.ApiService.getDetails().subscribe(data=>{
      this.users=data;
    });
  }

selectedUser;
getPosts(id){
  
 
   this.router.navigate(['/posts/'+id]);
   

  //this.router.navigate(['/Posts']);
    
}
/*goToPage(pageName:string){
  this.router.navigate([`${pageName}`]);
}*/
Comments;
getComments(id){
  this.ApiService.getComments(id).subscribe(data=>{
    
    this.Comments=data;})
}



}
