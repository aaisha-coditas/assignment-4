import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from "./user/user.component";
import {PostsComponent} from "./posts/posts.component";
import {CommentsComponent} from "./comments/comments.component";

const routes: Routes = [
  { path: 'users', component: UserComponent },
  { path: 'posts/:id', component: PostsComponent }
  //{ path: 'comments/:id', component: CommentsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
